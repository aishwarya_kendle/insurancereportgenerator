"""
-----------------------------------Data relation
- Agency --> Agent/Producer/CSR
- Category --> Type --> Description
-----------------------------------
----------------------------------- Current Section
currentSection == 0 [agency_agents]
currentSection == 1 [category]
rest all will be searched and inserted
"""

from openpyxl import load_workbook
import json

# Loading the xlsx file
wb = load_workbook('TIMESHEET_SMALL.xlsx', data_only=True)
data = wb["Data"]

# Global Variables
agency_agents = {}
category_type_description = {}
currentSection = 0

for column_cell in data.iter_cols(1, data.max_column):  # iterate column cell
    
    # Check if column is None, Gaps in columns to seperate the types
    if column_cell[0].value == None:
        currentSection += 1
        continue
    
    # ----------------------- Building agency_agents
    if currentSection == 0: 
        colName = column_cell[0].value
        agency_agents[colName] = []
        # Read Column rows
        for row in column_cell[1:]:    # iterate  column
            #  Read Till last data row
            if row.value == None:
                break
            agency_agents[colName].append(row.value)

    # ----------------------- Building category & type
    elif currentSection == 1:
        category = column_cell[0].value
        category_type_description[category] = {}
        # ----------------------- Building type
        # Read Column rows
        for typeName in column_cell[1:]:    # iterate  column
            #  Read Till last data row
            if typeName.value == None:
                break
            category_type_description[category][typeName.value] = []

    # ----------------------- Building description # TODO considering that the types are unique, might break if type is duplicated
    else:
        typeName = column_cell[0].value
        for key in category_type_description:
            # Search for the typeName under all categories
            if typeName in category_type_description[key]:
                 # typeName Found !
                for description in column_cell[1:]:    # iterate  column
                    #  Read Till last data row
                    if description.value == None:
                        break
                    category_type_description[key][typeName].append(description.value)

print(json.dumps(agency_agents, indent=4))
print(json.dumps(category_type_description, indent=4))

