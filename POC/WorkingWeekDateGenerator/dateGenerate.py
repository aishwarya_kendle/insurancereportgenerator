import datetime
import sys
month = int(sys.argv[1])

now = datetime.date(datetime.datetime.now().year, month, 1)

ls = {}
ls_lit = []
weekCounter = 0
weekCountIncremented = False
for i in range(1, 32):
    try:
        thisdate = datetime.date(now.year, now.month, i)
    except(ValueError):
        break
    if thisdate.weekday() < 5: # Monday == 0, Sunday == 6 
        ls_lit.append(thisdate.day)
        weekCountIncremented = False
    else:
        if ls_lit:
            ls[weekCounter] = ls_lit
            weekCounter += 1
        ls_lit = []
        if not weekCountIncremented : 
            weekCountIncremented = True

if ls_lit:
    ls[weekCounter] = ls_lit
print(ls)