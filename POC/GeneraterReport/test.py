# import pandas as pds

# file =('report.xlsx')
# newData = pds.read_excel(file, index_col=[0])
# # for row in newData:
# #     print(row)
# print(newData.to_markdown())
# newData.to_excel("op.xlsx")

from os import sep
from random import randint
import sys
from copy import deepcopy
from openpyxl import load_workbook, Workbook
from openpyxl.styles import Alignment, Font
from openpyxl.styles.fills import GradientFill, PatternFill
from openpyxl.styles.colors import BLACK, WHITE
from openpyxl.styles.borders import Border, Side
from generateReportData import printJSON
from generateReportData import getWeekDates, getQuoteSummary, getQuoteDetailedResult, readTimesheet, readData, getUtilizationForWeek, getAgentUtilization, getCountTable, getBOTasksSummary, getLegends

thin_border = Border(left=Side(style='thick'), right=Side(style='thick'), top=Side(style='thick'), bottom=Side(style='thick'))

master_row = 0

def read_excel_sheet(sheet_obj):
    for row in sheet_obj.iter_rows():
        for cell in row:
            print(cell.value, end="|")
        print("")


def read_workbook(filename):
    wb = load_workbook(filename = filename, data_only=True)
    summary_sheet = wb['Summary']
    read_excel_sheet(summary_sheet)


def header(work_sheet):
    cols = "ABCDEFGHIJKLMNOPQRSTU"
    for i in range(2, 67):
        for j in range(1, len(cols)+1):
            work_sheet.cell(i, j).fill = PatternFill(start_color='222b35', end_color='222b35', fill_type='solid')

    # First row "Monthly Timesheet"
    work_sheet.merge_cells('A1:U1')
    work_sheet['A1'] = 'Monthly Timesheet'
    cell = work_sheet['A1']
    cell.font = Font(size=14, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="right")
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, BLACK])

    # --------------------------------------------- Second row ---------------------------------------------
    # growth.insure
    work_sheet.merge_cells('B2:H2')
    work_sheet['B2'] = 'growth.insure'
    cell = work_sheet['B2']
    cell.font = Font(size=20, color='FFFFFF', bold=True)
    cell.fill = GradientFill(type='linear', degree=0, left=0, right=1, top=1, bottom=0, stop=['3498DB', WHITE])


    # month
    work_sheet.merge_cells('I2:M2')
    work_sheet['I2'] = 'growth.insure'
    cell = work_sheet['I2']
    cell.font = Font(size=20, color='3498DB', bold=True)
    cell.fill = GradientFill(type='linear', degree=0, left=0, right=1, top=1, bottom=0, stop=[WHITE, WHITE])


    # agency name
    work_sheet.merge_cells('N2:T2')
    work_sheet['N2'] = 'agency_name'
    cell = work_sheet['N2']
    cell.font = Font(size=20, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="right")
    cell.fill = GradientFill(type='linear', degree=180, left=0, right=1, top=1, bottom=0, stop=['3498DB', WHITE])


def quote_summary_table(work_sheet, timesheet, category_type_description):
    global master_row
    # --------------------------------------------- Third row ---------------------------------------------
    work_sheet.merge_cells('B3:T3')
    
    # --------------------------------------------- Quotes summary Table ---------------------------------------------
    # --------------------------------------------- Quotes summary Table Header ---------------------------------------------
    work_sheet.merge_cells('C{row}:F{row}'.format(row=str(master_row)))
    work_sheet['C'+str(master_row)] = 'QUOTES SUMMARY'
    cell = work_sheet['C'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, 'A6ACAF'])

    master_row += 1

    work_sheet['C'+str(master_row)] = 'Customer Type'
    cell = work_sheet['C'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['D'+str(master_row)] = 'Minutes'
    cell = work_sheet['D'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['E'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['E'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['F'+str(master_row)] = 'Count'
    cell = work_sheet['F'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    master_row += 1
    # --------------------------------------------- Quotes summary Table Data ---------------------------------------------
    work_sheet.font = Font(size=11, color='FFFFFF', bold=True) 
    quoteSummaryJSON = getQuoteSummary(timesheet, category_type_description)
    for key, value in quoteSummaryJSON["Customer Type"].items():
        work_sheet['C'+str(master_row)] = key
        work_sheet['C'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['C'+str(master_row)].border = thin_border


        work_sheet['D'+str(master_row)] = value['Minutes']
        work_sheet['D'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['D'+str(master_row)].border = thin_border
        work_sheet['D'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")


        work_sheet['E'+str(master_row)] = value['Hours: Minutes']
        work_sheet['E'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['E'+str(master_row)].border = thin_border
        work_sheet['E'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")


        work_sheet['F'+str(master_row)] = value['Count']
        work_sheet['F'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['F'+str(master_row)].border = thin_border
        work_sheet['F'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
    
        master_row += 1

    work_sheet['E'+str(master_row)] = 'Total'
    work_sheet['E'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['E'+str(master_row)].border = thin_border
    work_sheet['E'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['F'+str(master_row)] = quoteSummaryJSON['Summary']['totalCount']
    work_sheet['F'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['F'+str(master_row)].border = thin_border
    work_sheet['F'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
    # --------------------------------------------- Quotes summary Table Data End ---------------------------------------------

    master_row += 1


def quote_summary_detailed_table(work_sheet, timesheet, category_type_description):
    # --------------------------------------------- Line of business summary Table Data End ---------------------------------------------
    global master_row
    work_sheet['C'+str(master_row)] = 'Line of Business'
    cell = work_sheet['C'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['D'+str(master_row)] = 'Minutes'
    cell = work_sheet['D'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['E'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['E'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['F'+str(master_row)] = 'Count'
    cell = work_sheet['F'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    work_sheet['G'+str(master_row)] = 'Count Per LOB'
    cell = work_sheet['G'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    master_row += 1

    getQuoteDetailedJSON = getQuoteDetailedResult(timesheet, category_type_description)
    work_sheet.font = Font(size=11, color='FFFFFF', bold=True) 
    for key, value in getQuoteDetailedJSON["Line of Business"].items():
        work_sheet['C'+str(master_row)] = key
        work_sheet['C'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['C'+str(master_row)].border = thin_border
        work_sheet['C'+str(master_row)].border = thin_border


        work_sheet['D'+str(master_row)] = value['Minutes']
        work_sheet['D'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['D'+str(master_row)].border = thin_border
        work_sheet['D'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")
        work_sheet['D'+str(master_row)].border = thin_border


        work_sheet['E'+str(master_row)] = value['Hours: Minutes']
        work_sheet['E'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['E'+str(master_row)].border = thin_border
        work_sheet['E'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")
        work_sheet['E'+str(master_row)].border = thin_border


        work_sheet['F'+str(master_row)] = value['Count']
        work_sheet['F'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['F'+str(master_row)].border = thin_border
        work_sheet['F'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
        work_sheet['F'+str(master_row)].border = thin_border

        work_sheet['G'+str(master_row)] = value['Count Per LOB']
        work_sheet['G'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['G'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
        work_sheet['G'+str(master_row)].border = thin_border

        master_row += 1
    
    work_sheet['C'+str(master_row)] = "Total"
    work_sheet['C'+str(master_row)].font = Font(size=11, color='FFFFFF', bold=True) 
    work_sheet['C'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=BLACK)
    work_sheet['C'+str(master_row)].border = thin_border


    work_sheet['D'+str(master_row)] = getQuoteDetailedJSON["Summary"]['totalMinutesCount']
    work_sheet['D'+str(master_row)].font = Font(size=11, color='FFFFFF', bold=True) 
    work_sheet['D'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=BLACK)
    work_sheet['D'+str(master_row)].border = thin_border
    work_sheet['D'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")
    work_sheet['D'+str(master_row)].border = thin_border


    work_sheet['E'+str(master_row)] = getQuoteDetailedJSON["Summary"]['totalHoursMinutesCount']
    work_sheet['E'+str(master_row)].font = Font(size=11, color='FFFFFF', bold=True) 
    work_sheet['E'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=BLACK)
    work_sheet['E'+str(master_row)].border = thin_border
    work_sheet['E'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")
    work_sheet['E'+str(master_row)].border = thin_border


    work_sheet['F'+str(master_row)] = getQuoteDetailedJSON["Summary"]['totalCount']
    work_sheet['F'+str(master_row)].font = Font(size=11, color='FFFFFF', bold=True) 
    work_sheet['F'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=BLACK)
    work_sheet['F'+str(master_row)].border = thin_border
    work_sheet['F'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
    work_sheet['F'+str(master_row)].border = thin_border

    work_sheet['G'+str(master_row)] = getQuoteDetailedJSON["Summary"]['totalCountPerLOB']
    work_sheet['G'+str(master_row)].font = Font(size=11, color='FFFFFF', bold=True) 
    work_sheet['G'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=BLACK)
    work_sheet['G'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
    work_sheet['G'+str(master_row)].border = thin_border

    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    master_row += 1


def utilization_for_week_table(work_sheet, timesheet, category_type_description):
    global master_row

    work_sheet.merge_cells('I{row}:K{row}'.format(row=str(master_row)))
    work_sheet['I' + str(master_row)] = 'Utilization for Week'
    cell = work_sheet['I' + str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, 'E74C3C'])
    
    master_row += 1

    work_sheet['I'+str(master_row)] = 'Category'
    cell = work_sheet['I'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['J'+str(master_row)] = 'Total Time (in Mins)'
    cell = work_sheet['J'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['K'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['K'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    master_row += 1
    utilizationForWeekJSON = getUtilizationForWeek(timesheet, deepcopy(category_type_description))
    work_sheet.font = Font(size=11, color='FFFFFF', bold=True) 
    for key, value in utilizationForWeekJSON["Category"].items():
        work_sheet['I'+str(master_row)] = key
        cell = work_sheet['I'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor=WHITE)
        cell.border = thin_border


        work_sheet['J'+str(master_row)] = value['Minutes']
        cell = work_sheet['J'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor=WHITE)
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border


        work_sheet['K'+str(master_row)] = value['Hours: Minutes']
        cell = work_sheet['K'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor=WHITE)
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        master_row += 1

    work_sheet['I'+str(master_row)] = "Consumed Time for Week"
    cell = work_sheet['I'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.border = thin_border
    cell.border = thin_border


    work_sheet['J'+str(master_row)] = utilizationForWeekJSON["Summary"]['Consumed Time-Minute for Week']
    cell = work_sheet['J'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.border = thin_border
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.border = thin_border


    work_sheet['K'+str(master_row)] = utilizationForWeekJSON["Summary"]['Consumed Time-Hour for Week']
    cell = work_sheet['K'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.border = thin_border
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.border = thin_border

    master_row += 1


def countTable(work_sheet, timesheet, category_type_description):
    global master_row

    work_sheet.merge_cells('I{row}:K{row}'.format(row=str(master_row)))
    work_sheet['I' + str(master_row)] = 'Count (No. of)'
    cell = work_sheet['I' + str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, 'E74C3C'])
    
    master_row += 1

    work_sheet['I'+str(master_row)] = 'Category'
    cell = work_sheet['I'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.border = thin_border


    work_sheet.merge_cells('J{row}:K{row}'.format(row=str(master_row)))
    work_sheet['J'+str(master_row)] = 'Count'
    cell = work_sheet['J'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    master_row += 1

    utilizationForWeekJSON = getCountTable(timesheet, deepcopy(category_type_description))
    work_sheet.font = Font(size=11, color='FFFFFF', bold=True) 
    for key, value in utilizationForWeekJSON.items():
        work_sheet['I'+str(master_row)] = key
        cell = work_sheet['I'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor=WHITE)
        cell.border = thin_border


        work_sheet.merge_cells('J{row}:K{row}'.format(row=str(master_row)))
        work_sheet['J'+str(master_row)] = value
        cell = work_sheet['J'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor=WHITE)
        cell.alignment = Alignment(horizontal="center" , vertical="bottom")
        cell.border = thin_border

        cell = work_sheet['K'+str(master_row)]
        cell.border = thin_border

        master_row += 1


def agent_utilization(work_sheet, timesheet, agency_agents):
    global master_row

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B' + str(master_row)] = 'Agent Utilization'
    cell = work_sheet['B' + str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, 'A6ACAF'])

    master_row += 1

    work_sheet['B'+str(master_row)] = 'Agent/CSR'
    cell = work_sheet['B'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.border = thin_border

    work_sheet['C'+str(master_row)] = 'Quote Time (Mins)'
    cell = work_sheet['C'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border
    
    work_sheet['D'+str(master_row)] = 'BO Time (Mins)'
    cell = work_sheet['D'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    work_sheet['E'+str(master_row)] = 'Total Time (in Mins)'
    cell = work_sheet['E'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    work_sheet['F'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['F'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    work_sheet['G'+str(master_row)] = 'Quotes Count'
    cell = work_sheet['G'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    work_sheet['H'+str(master_row)] = 'BO Tasks Count'
    cell = work_sheet['H'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    master_row += 1

    agentUtilizationJSON = getAgentUtilization(timesheet, agency_agents)
    work_sheet.font = Font(size=11, color='FFFFFF', bold=True) 
    for key, value in agentUtilizationJSON.items():
        work_sheet['B'+str(master_row)] = key
        cell = work_sheet['B'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.border = thin_border

        work_sheet['C'+str(master_row)] = value['quoteTime']
        cell = work_sheet['C'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border
        
        work_sheet['D'+str(master_row)] = value['BOTime']
        cell = work_sheet['D'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['E'+str(master_row)] = value['totalTime']
        cell = work_sheet['E'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['F'+str(master_row)] = value['Hours: Minutes']
        cell = work_sheet['F'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['G'+str(master_row)] = value['quotesCount']
        cell = work_sheet['G'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['H'+str(master_row)] = value['BOTasksCount']
        cell = work_sheet['H'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        master_row += 1


def bo_task_summary(work_sheet, timesheet, category_type_description):
    global master_row

    work_sheet.merge_cells('M{row}:P{row}'.format(row=str(master_row)))
    work_sheet['M'+str(master_row)] = 'BO TASKS SUMMARY'
    cell = work_sheet['M'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, '00FF00'])

    master_row += 1

    work_sheet['M'+str(master_row)] = 'BO Tasks'
    cell = work_sheet['M'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['N'+str(master_row)] = 'Minutes'
    cell = work_sheet['N'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['O'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['O'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.border = thin_border

    work_sheet['P'+str(master_row)] = 'Count'
    cell = work_sheet['P'+str(master_row)]
    cell.font = Font(size=11, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center" , vertical="bottom")
    cell.border = thin_border

    master_row += 1
    # --------------------------------------------- Quotes summary Table Data ---------------------------------------------
    work_sheet.font = Font(size=11, color='FFFFFF', bold=True) 
    boTaskSummaryJSON = getBOTasksSummary(timesheet, category_type_description)
    for key, value in boTaskSummaryJSON["BO Tasks"].items():
        work_sheet['M'+str(master_row)] = key
        work_sheet['M'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['N'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
        work_sheet['M'+str(master_row)].border = thin_border


        work_sheet['N'+str(master_row)] = value['Minutes']
        work_sheet['N'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['N'+str(master_row)].border = thin_border
        work_sheet['N'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")


        work_sheet['O'+str(master_row)] = value['Hours: Minutes']
        work_sheet['O'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['O'+str(master_row)].border = thin_border
        work_sheet['O'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")


        work_sheet['P'+str(master_row)] = value['Count']
        work_sheet['P'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['P'+str(master_row)].border = thin_border
        work_sheet['P'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
    
        master_row += 1

    work_sheet['M'+str(master_row)] = 'Total'
    work_sheet['M'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['M'+str(master_row)].border = thin_border
    work_sheet['M'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")

    work_sheet['N'+str(master_row)] = boTaskSummaryJSON['Summary']['totalMinutesCount']
    work_sheet['N'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['N'+str(master_row)].border = thin_border
    work_sheet['N'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")

    work_sheet['O'+str(master_row)] = boTaskSummaryJSON['Summary']['totalHoursMinutesCount']
    work_sheet['O'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['O'+str(master_row)].border = thin_border
    work_sheet['O'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")

    work_sheet['P'+str(master_row)] = boTaskSummaryJSON['Summary']['totalCount']
    work_sheet['P'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['P'+str(master_row)].border = thin_border
    work_sheet['P'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
    # --------------------------------------------- Quotes summary Table Data End ---------------------------------------------

    master_row += 1
    pass


def legends_used(work_sheet):
    global master_row

    work_sheet.merge_cells('M{row}:P{row}'.format(row=str(master_row)))
    work_sheet['M'+str(master_row)] = '* LEGENDS USED'
    cell = work_sheet['M'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = Alignment(horizontal="center")
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, '00FF00'])

    master_row += 1

    work_sheet.font = Font(size=11, color='FFFFFF', bold=True) 
    legendsJSON = getLegends()
    for key, value in legendsJSON.items():
        work_sheet['M'+str(master_row)] = key
        work_sheet['M'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['N'+str(master_row)].alignment = Alignment(horizontal="center" , vertical="bottom")
        work_sheet['M'+str(master_row)].border = thin_border

        work_sheet.merge_cells('N{row}:P{row}'.format(row=str(master_row)))
        work_sheet['N'+str(master_row)] = value
        work_sheet['N'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['N'+str(master_row)].alignment = Alignment(horizontal="left" , vertical="bottom")

        master_row += 1

def write_merged_cells(agency_name, timesheet, agency_agents, category_type_description, work_sheet):
    global master_row
    # wb = Workbook()
    # dest_filename = 'op' + str(randint(0, 99)) + ".xlsx"
    
    # work_sheet = wb.active
    
    # work_sheet.title = "Summary"
    
    header(work_sheet)

    master_row = 8
    quote_summary_table(work_sheet, deepcopy(timesheet), deepcopy(category_type_description))
    quote_summary_detailed_table(work_sheet, deepcopy(timesheet), deepcopy(category_type_description))
    master_row += 1
    agent_utilization(work_sheet, deepcopy(timesheet), agency_agents)

    master_row = 8
    utilization_for_week_table(work_sheet, deepcopy(timesheet), deepcopy(category_type_description))
    master_row += 3
    countTable(work_sheet, timesheet, deepcopy(category_type_description))

    master_row = 8
    bo_task_summary(work_sheet, timesheet, deepcopy(category_type_description))
    legends_used(work_sheet)
    # wb.save(filename = dest_filename)
    
    

# Read timesheet and database
wb = load_workbook(sys.argv[1], data_only=True)
timesheet = readTimesheet(wb["Month"])
agency_agents, category_type_description = readData(wb["Data"])

agency_name = "Excellent Service Insurance Agency"

month = getWeekDates()

wb = Workbook()
dest_filename = 'op' + str(randint(0, 99)) + ".xlsx"
for week in month:
    worksheet = wb.create_sheet('{}-{}'.format(month[week][0], month[week][-1]))    
    write_merged_cells(agency_name, timesheet[week], agency_agents, category_type_description, worksheet)



    wb.save(filename = dest_filename)
