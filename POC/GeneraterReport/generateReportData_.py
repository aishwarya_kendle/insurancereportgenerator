from itertools import count
from openpyxl import load_workbook
import sys
import json
from datetime import datetime
from datetime import timedelta

#  Reading the excel sheet
wb = load_workbook(sys.argv[1], data_only=True)

# -------------------------------------------- Utilities
def printJSON(data):
    print(json.dumps(data, indent=4, default=str))

def convertMinutesToTimeString(minutes):
    hours = minutes // 60
    minutes = minutes % 60
    return "{}:{}".format(hours, minutes)

def addTime(time1, time2):
    
    pass
# -------------------------------------------- Read DATA
def readTimesheet(month):
    # Global Variables
    headerRow = None
    headerFound = False
    rows = []


    for row in month.iter_rows(min_row=1, max_col=13, values_only=True):
        # Save the header row
        if(row[0]=="Date"):
            headerRow = row
            headerFound = True
            # print("Header Found  : ", headerRow)
            continue
            
        # Skipping the next row after header row as it is merged
        if headerRow:
            if headerFound:
                headerFound = False
                continue

            # Read row until row[0] is not None
            if row[0] != None:
                tempRow = {}        
                # Creating a dict of colName : Value
                for i in range(0, len(row)):
                    tempRow[headerRow[i]] = row[i]        
                rows.append(tempRow)

    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(rows)
    return rows

def readData(data):
    # Global Variables
    agency_agents = {}
    category_type_description = {}
    currentSection = 0

    for column_cell in data.iter_cols(1, data.max_column):  # iterate column cell
        
        # Check if column is None, Gaps in columns to seperate the types
        if column_cell[0].value == None:
            currentSection += 1
            continue
        
        # ----------------------- Building agency_agents
        if currentSection == 0: 
            colName = column_cell[0].value
            agency_agents[colName] = []
            # Read Column rows
            for row in column_cell[1:]:    # iterate  column
                #  Read Till last data row
                if row.value == None:
                    break
                agency_agents[colName].append(row.value)

        # ----------------------- Building category & type
        elif currentSection == 1:
            category = column_cell[0].value
            category_type_description[category] = {}
            # ----------------------- Building type
            # Read Column rows
            for typeName in column_cell[1:]:    # iterate  column
                #  Read Till last data row
                if typeName.value == None:
                    break
                category_type_description[category][typeName.value] = []

        # ----------------------- Building description # TODO considering that the types are unique, might break if type is duplicated
        else:
            typeName = column_cell[0].value
            for key in category_type_description:
                # Search for the typeName under all categories
                if typeName in category_type_description[key]:
                    # typeName Found !
                    for description in column_cell[1:]:    # iterate  column
                        #  Read Till last data row
                        if description.value == None:
                            break
                        category_type_description[key][typeName].append(description.value)

    # print(json.dumps(agency_agents, indent=4))
    # print(json.dumps(category_type_description, indent=4))
    return agency_agents, category_type_description

# -------------------------------------------- Generate Report Data
# Generating QuoteSummary
def getQuoteSummary(timesheet, category_type_description):
    totalCount = 0
    quoteFields = category_type_description["Quote"]
    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0",
        "Count": 0
    }
    for key in quoteFields:
        quoteFields[key] = dataFields.copy()
    
    for row in timesheet:
        if row["Type"] in quoteFields:
            quoteFields[row["Type"]]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            quoteFields[row["Type"]]["Hours: Minutes"] = convertMinutesToTimeString(quoteFields[row["Type"]]["Minutes"])
            quoteFields[row["Type"]]["Count"] += 1
            totalCount += 1
           
    quoteSummary = {
        "Customer Type": quoteFields,
        "Summary":{
            "totalCount" : totalCount
        }
    }

    """
    {
        "Customer Type": {
            "NC_Brand_New": {
                "Minutes": 70,
                "Hours: Minutes": "1:10",
                "Count": 1
            },
            "NC_Returning": {
                "Minutes": 0,
                "Hours: Minutes": 0,
                "Count": 0
            },
            "EC_New_LOB": {
                "Minutes": 0,
                "Hours: Minutes": 0,
                "Count": 0
            },
            "EC_Renewal_Requote": {
                "Minutes": 0,
                "Hours: Minutes": 0,
                "Count": 0
            }
        },
        "Summary": {
            "totalCount": 1
        }
    }
    """
    return quoteSummary
def getQuoteDetailedResult(timesheet, category_type_description):
    totalMinutesCount = 0
    totalHoursMinutesCount = 0
    totalCount = 0
    totalCountPerLOB = 0
    
    highPriorityTasks = 0
    mediumPriorityTasks = 0
    lowPriorityTasks = 0

    LOBFieldsList = []
    LOBFields = {}
    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0",
        "Count": 0,
        "Count Per LOB": 0
    }
    # Getting LOB data
    for key in category_type_description["Quote"]:
        LOBFieldsList = category_type_description["Quote"][key]
        break

    for value in LOBFieldsList:
        LOBFields[value] = dataFields.copy()

    # Filling LOB table data
    for row in timesheet:
        if row["Category"] == "Quote":
            if row["Quote/Task Priority"] == "High":
                highPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Medium":
                mediumPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Low":
                lowPriorityTasks += 1

        if row["Description"] in LOBFields:
            LOBFields[row["Description"]]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            LOBFields[row["Description"]]["Hours: Minutes"] = convertMinutesToTimeString(LOBFields[row["Description"]]["Minutes"])
            LOBFields[row["Description"]]["Count"] += 1

            if row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"] != None:
                LOBFields[row["Description"]]["Count Per LOB"] += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]
                totalCountPerLOB += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]

            totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]
            totalCount += 1
    
    totalHoursMinutesCount = convertMinutesToTimeString(totalMinutesCount)
  
    quoteDetailedResult = {
        "Line of Business": LOBFields,
        "Summary":{
            "totalMinutesCount": totalMinutesCount,
            "totalHoursMinutesCount": totalHoursMinutesCount,
            "totalCount": totalCount,
            "totalCountPerLOB": totalCountPerLOB,
            "highPriorityTasks": highPriorityTasks,
            "mediumPriorityTasks": mediumPriorityTasks,
            "lowPriorityTasks": lowPriorityTasks
        }
    }
    """
        {
            "Line of Business": {
                "Auto": {
                    "Minutes": 264,
                    "Hours: Minutes": "4:24",
                    "Count": 5,
                    "Count Per LOB": 5
                },
                "Home": {
                    "Minutes": 654,
                    "Hours: Minutes": "10:54",
                    "Count": 11,
                    "Count Per LOB": 14
                },
                "Renters": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Condo": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Home": {
                    "Minutes": 2700,
                    "Hours: Minutes": "45:0",
                    "Count": 26,
                    "Count Per LOB": 58
                },
                "Auto+Renters": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Condo": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Home+DF": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Dwelling_Fire": {
                    "Minutes": 845,
                    "Hours: Minutes": "14:5",
                    "Count": 14,
                    "Count Per LOB": 14
                },
                "Landlord_Condo": {
                    "Minutes": 275,
                    "Hours: Minutes": "4:35",
                    "Count": 5,
                    "Count Per LOB": 5
                },
                "Umbrella": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Home+Umbrella": {
                    "Minutes": 730,
                    "Hours: Minutes": "12:10",
                    "Count": 7,
                    "Count Per LOB": 27
                },
                "Auto+Umbrella": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Renters+Umbrella": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "EZLynx_Quick_Review": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Motorcycle": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Trailer": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Vacant_Dwelling": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Earthquake": {
                    "Minutes": 28,
                    "Hours: Minutes": "0:28",
                    "Count": 1,
                    "Count Per LOB": 1
                },
                "Boat": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Flood": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Commercial": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Life_Quote": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                }
            },
            "Summary": {
                "totalMinutesCount": 5496,
                "totalHoursMinutesCount": "91:36",
                "totalCount": 69,
                "totalCountPerLOB": 124
            }
        }
    """
    # printJSON(LOBFields)
    return quoteDetailedResult

def getBOTasksSummary(timesheet, category_type_description):
    totalMinutesCount = 0
    totalHoursMinutesCount = 0
    totalCount = 0
    
    highPriorityTasks = 0
    mediumPriorityTasks = 0
    lowPriorityTasks = 0

    BOTasks = {}
    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0",
        "Count": 0
    }


    # Build Placeholder
    for key in category_type_description["BO"]:
        for task in category_type_description["BO"][key]:
            BOTasks[task] = dataFields.copy()

    # Filling LOB table data
    for row in timesheet:
        if row["Category"] == "BO":
            if row["Quote/Task Priority"] == "High":
                highPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Medium":
                mediumPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Low":
                lowPriorityTasks += 1
            
        if row["Description"] in BOTasks:
            BOTasks[row["Description"]]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            BOTasks[row["Description"]]["Hours: Minutes"] = convertMinutesToTimeString(BOTasks[row["Description"]]["Minutes"])
            BOTasks[row["Description"]]["Count"] += 1

            totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]
            totalCount += 1

    totalHoursMinutesCount = convertMinutesToTimeString(totalMinutesCount)
    BOTasksSummary = {
        "BO Tasks": BOTasks,
        "Summary":{
            "totalMinutesCount": totalMinutesCount,
            "totalHoursMinutesCount": totalHoursMinutesCount,
            "totalCount": totalCount,
            "highPriorityTasks": highPriorityTasks,
            "mediumPriorityTasks": mediumPriorityTasks,
            "lowPriorityTasks": lowPriorityTasks
        }
    }
    """
        {
            "Line of Business": {
                "Application_Creation": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "COI_Printing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "DocuSign": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Audit": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Carrier": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Inbound": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Outbound": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_HawkSoft": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Other": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Chat_with_Agent": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Chat_with_Team": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_with_Agent": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_with_Team": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Download_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Downloads_Run": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Policy_Tab_Creation": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Unmatched_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Email_Sorting": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Email_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Email_Monitoring": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Marketing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Referral": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Touch_Point": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Miscellaneous_Billable": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Change_Mortgage_Lien": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Change_Policy": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Policy_Issue": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Payment_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Quote_Request_Creation": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_Agency_Growth": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_EzLynx": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_HawkSoft": {
                    "Minutes": 1500,
                    "Hours: Minutes": "25:0",
                    "Count": 100
                },
                "Reports_Monthly_Renewal": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_Policy_Activity": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_Policy_Count": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Suspense_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Tech_Support_To_Agent": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Quote_Review": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                }
            },
            "Summary": {
                "totalMinutesCount": 1500,
                "totalHoursMinutesCount": "25:0",
                "totalCount": 100
            }
        }
    """
    return BOTasksSummary

def getUtilizationForWeek(timesheet, category_type_description):    
    totalMinutesCount = 0
    totalHoursMinutesCount = 0

    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0"        
    }

    category = {
        "Application_Creation": dataFields.copy(),
        "Audit": dataFields.copy(),
        "Total Quotes": dataFields.copy(),
        "Total BO Tasks": dataFields.copy()
    }

    # Filling category table data
    for row in timesheet:
        if "Application_Creation" in row["Description"]:
            category["Application_Creation"]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            category["Application_Creation"]["Hours: Minutes"] = convertMinutesToTimeString(category["Application_Creation"]["Minutes"])

            totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]
        elif "Audit" in row["Description"]:
            category["Audit"]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            category["Audit"]["Hours: Minutes"] = convertMinutesToTimeString(category["Audit"]["Minutes"])

            totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]

    quotesData = getQuoteDetailedResult(timesheet, category_type_description)

    category["Total Quotes"]["Minutes"] = quotesData["Summary"]["totalMinutesCount"]
    category["Total Quotes"]["Hours: Minutes"] = quotesData["Summary"]["totalHoursMinutesCount"]
    totalMinutesCount += quotesData["Summary"]["totalMinutesCount"]

    BOdata = getBOTasksSummary(timesheet, category_type_description)

    category["Total BO Tasks"]["Minutes"] = BOdata["Summary"]["totalMinutesCount"]
    category["Total BO Tasks"]["Hours: Minutes"] = BOdata["Summary"]["totalHoursMinutesCount"]
    totalMinutesCount += BOdata["Summary"]["totalMinutesCount"]

    totalHoursMinutesCount = convertMinutesToTimeString(totalMinutesCount)

    categoryData = {
        "Category": category,
        "Summary":{
            "Consumed Time-Minute for Week": totalMinutesCount,
            "Consumed Time-Hour for Week": totalHoursMinutesCount
        }
    }

    """
    {
        "Category": {
            "Application_Creation": {
                "Minutes": 0,
                "Hours: Minutes": "0:0"
            },
            "Audit": {
                "Minutes": 0,
                "Hours: Minutes": "0:0"
            },
            "Total Quotes": {
                "Minutes": 5496,
                "Hours: Minutes": "91:36"
            },
            "Total BO Tasks": {
                "Minutes": 1500,
                "Hours: Minutes": "25:0"
            }
        },
        "Summary": {
            "Consumed Time-Minute for Week": 6996,
            "Consumed Time-Hour for Week": "116:36"
        }
    }
    """

    return categoryData

def getCountTable(timesheet, category_type_description):
    quoteDetailedResult = getQuoteDetailedResult(timesheet, category_type_description)
    BOTasksSummary = getBOTasksSummary(timesheet, category_type_description)
    
    countTable = {}
    
    applicationsCreated = 0
    policiesAudited = 0

    for row in timesheet:
        if "Application_Creation" in row["Description"]:
            applicationsCreated += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]

        elif "Audit" in row["Description"]:
            policiesAudited += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]

    countTable["Total Quotes (Households)"] =  quoteDetailedResult["Summary"]["totalCount"]
    countTable["Total Quotes (LOBs)"] =  quoteDetailedResult["Summary"]["totalCountPerLOB"]
    countTable["Total BO Tasks"] =  BOTasksSummary["Summary"]["totalCount"]
    countTable["Applications Created"] =  applicationsCreated
    countTable["Policies Audited"] =  policiesAudited
    countTable["High Priority Quotes"] =  quoteDetailedResult["Summary"]["highPriorityTasks"]
    countTable["Medium Priority Quotes"] =  quoteDetailedResult["Summary"]["mediumPriorityTasks"]
    countTable["Low Priority Quotes"] =  quoteDetailedResult["Summary"]["lowPriorityTasks"]
    countTable["High Priority BO Tasks"] =  BOTasksSummary["Summary"]["highPriorityTasks"]
    countTable["Medium Priority BO Tasks"] =  BOTasksSummary["Summary"]["mediumPriorityTasks"]
    countTable["Low Priority BO Tasks"] =  BOTasksSummary["Summary"]["lowPriorityTasks"]

    """
    {
        "Total Quotes (Households)": 69,
        "Total Quotes (LOBs)": 124,
        "Total BO Tasks": 100,
        "Applications Created": 0,
        "Policies Audited": 0,
        "High Priority Quotes": 43,
        "Medium Priority Quotes": 0,
        "Low Priority Quotes": 26,
        "High Priority BO Tasks": 0,
        "Medium Priority BO Tasks": 100,
        "Low Priority BO Tasks": 0
    }
    """

    

    return countTable

def getAgentUtilization(timesheet, agency_agents):
    agency = timesheet[0]["Agency"]
    agents = agency_agents[agency]

    agentUtilization = {}

    dataFields = {
        "quoteTime": 0,
        "BOTime": 0,
        "totalTime": 0,
        "Hours: Minutes": "0:0",
        "quotesCount": 0,
        "BOTasksCount": 0
    }

    # Build Placeholder
    for agent in agents:
        agentUtilization[agent] = dataFields.copy()
        
    for row in timesheet:
        if row["Agent/Producer/CSR"] in agents:
            if row["Category"] == "Quote":
                agentUtilization[row["Agent/Producer/CSR"]]["quoteTime"] += row["Total Time taken by quote/task (in Minutes)"]    
                agentUtilization[row["Agent/Producer/CSR"]]["quotesCount"] += 1
            elif row["Category"] == "BO":
                agentUtilization[row["Agent/Producer/CSR"]]["BOTime"] += row["Total Time taken by quote/task (in Minutes)"]    
                agentUtilization[row["Agent/Producer/CSR"]]["BOTasksCount"] += 1

            agentUtilization[row["Agent/Producer/CSR"]]["totalTime"] = agentUtilization[row["Agent/Producer/CSR"]]["quoteTime"] + agentUtilization[row["Agent/Producer/CSR"]]["BOTime"]
            agentUtilization[row["Agent/Producer/CSR"]]["Hours: Minutes"] = convertMinutesToTimeString(agentUtilization[row["Agent/Producer/CSR"]]["totalTime"] )

    """
    {
        "growth.insure": {
            "quoteTime": 2740,
            "BOTime": 0,
            "totalTime": 2740,
            "Hours: Minutes": "45:40",
            "quotesCount": 34,
            "BOTasksCount": 0
        },
        "Karandikar, Smita": {
            "quoteTime": 929,
            "BOTime": 0,
            "totalTime": 929,
            "Hours: Minutes": "15:29",
            "quotesCount": 11,
            "BOTasksCount": 0
        },
        "Natu, Aparna": {
            "quoteTime": 98,
            "BOTime": 0,
            "totalTime": 98,
            "Hours: Minutes": "1:38",
            "quotesCount": 3,
            "BOTasksCount": 0
        },
        "Sathaye, Kiran": {
            "quoteTime": 300,
            "BOTime": 300,
            "totalTime": 600,
            "Hours: Minutes": "10:0",
            "quotesCount": 4,
            "BOTasksCount": 20
        },
        "Sutrave, Seema": {
            "quoteTime": 1409,
            "BOTime": 0,
            "totalTime": 1409,
            "Hours: Minutes": "23:29",
            "quotesCount": 16,
            "BOTasksCount": 0
        }
    }
    """
    return agentUtilization

def getLegends():

    legend = {
        "NC_Brand_New": "Complete Brand New Customer to Agency",		
        "NC_Returning":	"Returning Customer",		
        "EC_New_LOB":	"Existing Customer adding new LOB",		
        "EC_Renewal_Requote":	"Existing Customer - Requote/Remarket/Reshop"		
    }

    return legend

# Read timesheet and database
timesheet = readTimesheet(wb["Month"])
agency_agents, category_type_description = readData(wb["Data"])

# printJSON(getQuoteSummary(timesheet, category_type_description))
# printJSON(getQuoteDetailedResult(timesheet, category_type_description))
# printJSON(getBOTasksSummary(timesheet, category_type_description))
# printJSON(getUtilizationForWeek(timesheet, category_type_description))
# printJSON(getCountTable(timesheet, category_type_description))
# printJSON(getCountTable(timesheet, category_type_description))
# printJSON(getAgentUtilization(timesheet, agency_agents))
# print(agency_agents)
# printJSON(getLegends())

