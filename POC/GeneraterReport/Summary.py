from os import sep
from random import randint
import sys
from copy import deepcopy
from openpyxl import load_workbook, Workbook
from openpyxl.styles import Alignment, Font, NamedStyle
from openpyxl.styles.fills import GradientFill, PatternFill
from openpyxl.styles.colors import BLACK, WHITE
from openpyxl.styles.borders import Border, Side
from generateReportData import printJSON
from generateReportData import getQuoteSummary, getTotalSummaryData, readTimesheet, readData,  getLegends

thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))

all_thin_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
all_thick_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thin'))
lr_thick_border = Border(left=Side(style='thick'), right=Side(style='thick'), top=Side(style='thin'), bottom=Side(style='thin'))
b_thick_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thin'), bottom=Side(style='thick'))
t_thick_border = Border(left=Side(style='thin'), right=Side(style='thin'), top=Side(style='thick'), bottom=Side(style='thin'))

center_allign = Alignment(horizontal="center")
left_allign = Alignment(horizontal="left")
right_allign = Alignment(horizontal="right")

bold_font_11 = Font(size=11, color='FFFFFF', bold=True) 
bold_font_14 = Font(size=14, color='FFFFFF', bold=True) 
bold_font_18 = Font(size=18, color='FFFFFF', bold=True) 
bold_font_20 = Font(size=20, color='FFFFFF', bold=True) 

master_row = 0


def header(work_sheet):
    cols = "ABCDEFGHIJKLMNOPQRSTU"
    for i in range(2, 67):
        for j in range(1, len(cols)+1):
            work_sheet.cell(i, j).fill = PatternFill(start_color='222b35', end_color='222b35', fill_type='solid')

    # First row "Monthly Timesheet"
    work_sheet.merge_cells('A1:U1')
    work_sheet['A1'] = 'Monthly Timesheet'
    cell = work_sheet['A1']
    cell.font = bold_font_14
    cell.alignment = center_allign
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, BLACK])

    # --------------------------------------------- Second row ---------------------------------------------
    # growth.insure
    work_sheet.merge_cells('B2:H2')
    work_sheet['B2'] = 'growth.insure'
    cell = work_sheet['B2']
    cell.font = bold_font_20
    cell.fill = GradientFill(type='linear', degree=0, left=0, right=1, top=1, bottom=0, stop=['3498DB', WHITE])


    # month
    work_sheet.merge_cells('I2:M2')
    work_sheet['I2'] = 'growth.insure'
    cell = work_sheet['I2']
    cell.font = bold_font_20
    cell.fill = GradientFill(type='linear', degree=0, left=0, right=1, top=1, bottom=0, stop=[WHITE, WHITE])


    # agency name
    work_sheet.merge_cells('N2:T2')
    work_sheet['N2'] = 'agency_name'
    cell = work_sheet['N2']
    cell.font = bold_font_20 
    cell.alignment = right_allign
    cell.fill = GradientFill(type='linear', degree=180, left=0, right=1, top=1, bottom=0, stop=['3498DB', WHITE])

    work_sheet.merge_cells('B3:T3')


def quote_summary_table_header(work_sheet):
    global master_row
    
    # --------------------------------------------- Quotes summary Table ---------------------------------------------
    # --------------------------------------------- Quotes summary Table Header ---------------------------------------------
    work_sheet.merge_cells('B{row}:J{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'QUOTES SUMMARY'
    cell = work_sheet['B'+str(master_row)]
    cell.font = bold_font_18
    cell.alignment = center_allign
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, '484848'])

    master_row += 1

    work_sheet.merge_cells('B{}:B{}'.format(str(master_row), str(master_row + 1)))
    work_sheet['B'+str(master_row)] = 'Customer Types'
    cell = work_sheet['B'+str(master_row)]
    cell.font = bold_font_11 
    cell.alignment = center_allign
    cell.fill = PatternFill(start_color='707070', end_color='707070', fill_type='solid')
    cell.border = thin_border

    work_sheet.merge_cells('C{row}:G{row}'.format(row=str(master_row)))
    work_sheet['C'+str(master_row)] = 'Weekly Time (Minutes)'
    cell = work_sheet['C'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet.merge_cells('H{row}:I{row}'.format(row=str(master_row)))
    work_sheet['H'+str(master_row)] = 'Total Time'
    cell = work_sheet['H'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet.merge_cells('J{}:J{}'.format(str(master_row), str(master_row + 1)))
    work_sheet['J'+str(master_row)] = 'Count'
    cell = work_sheet['J'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    master_row += 1

    work_sheet['C'+str(master_row)] = 'Week 1'
    cell = work_sheet['C'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border


    work_sheet['D'+str(master_row)] = 'Week 2'
    cell = work_sheet['D'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['E'+str(master_row)] = 'Week 3'
    cell = work_sheet['E'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['F'+str(master_row)] = 'Week 4'
    cell = work_sheet['F'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['G'+str(master_row)] = 'Week 5'
    cell = work_sheet['G'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['H'+str(master_row)] = 'Minutes'
    cell = work_sheet['H'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['I'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['I'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    master_row += 1


def bo_summary_table_header(work_sheet):
    global master_row

    work_sheet.merge_cells('L{row}:T{row}'.format(row=str(master_row)))
    work_sheet['L'+str(master_row)] = 'BO TASKS SUMMARY'
    cell = work_sheet['L'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, '548235'])

    master_row += 1

    work_sheet.merge_cells('L{}:L{}'.format(str(master_row), str(master_row + 1)))
    work_sheet['L'+str(master_row)] = 'Task Type'
    cell = work_sheet['L'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.fill = PatternFill(patternType='solid', fgColor='548235')
    cell.border = thin_border

    work_sheet.merge_cells('M{row}:Q{row}'.format(row=str(master_row)))
    work_sheet['M'+str(master_row)] = 'Weekly Time (Minutes)'
    cell = work_sheet['M'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet.merge_cells('R{row}:S{row}'.format(row=str(master_row)))
    work_sheet['R'+str(master_row)] = 'Total Time'
    cell = work_sheet['R'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet.merge_cells('T{}:T{}'.format(str(master_row), str(master_row + 1)))
    work_sheet['T'+str(master_row)] = 'Count'
    cell = work_sheet['T'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    master_row += 1

    work_sheet['M'+str(master_row)] = 'Week 1'
    cell = work_sheet['M'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border


    work_sheet['N'+str(master_row)] = 'Week 2'
    cell = work_sheet['N'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['O'+str(master_row)] = 'Week 3'
    cell = work_sheet['O'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['P'+str(master_row)] = 'Week 4'
    cell = work_sheet['P'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['Q'+str(master_row)] = 'Week 5'
    cell = work_sheet['Q'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['R'+str(master_row)] = 'Total Minutes'
    cell = work_sheet['R'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['S'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['S'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = center_allign
    cell.border = thin_border

    master_row += 1


def bo_summary_table(work_sheet, summaryJSON):
    global master_row

    work_sheet.font = bold_font_11
    for key, value in summaryJSON["taskType"].items():
        # print(key, value, end="\t")
        # print(value[0], end="\n\n")

        work_sheet['L'+str(master_row)] = key
        work_sheet['L'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['L'+str(master_row)].border = thin_border

        work_sheet['M'+str(master_row)] = value["weeks"][0]
        work_sheet['M'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['M'+str(master_row)].border = thin_border
        work_sheet['M'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['N'+str(master_row)] = value["weeks"][1]
        work_sheet['N'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['N'+str(master_row)].border = thin_border
        work_sheet['N'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['O'+str(master_row)] = value["weeks"][2]
        work_sheet['O'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['O'+str(master_row)].border = thin_border
        work_sheet['O'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['P'+str(master_row)] = value["weeks"][3]
        work_sheet['P'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['P'+str(master_row)].border = thin_border
        work_sheet['P'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['Q'+str(master_row)] = value["weeks"][4]
        work_sheet['Q'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['Q'+str(master_row)].border = thin_border
        work_sheet['Q'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['R'+str(master_row)] = value['Minutes']
        work_sheet['R'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['R'+str(master_row)].border = thin_border
        work_sheet['R'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['S'+str(master_row)] = value['Hours: Minutes']
        work_sheet['S'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['S'+str(master_row)].border = thin_border
        work_sheet['S'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['T'+str(master_row)] = value['Count']
        work_sheet['T'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['T'+str(master_row)].border = thin_border
        work_sheet['T'+str(master_row)].alignment = center_allign

        master_row += 1

    value = summaryJSON['Summary']
    work_sheet['L'+str(master_row)] = 'Total'
    work_sheet['L'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['L'+str(master_row)].border = thin_border
    
    work_sheet['M'+str(master_row)] = value["weeks"][0]
    work_sheet['M'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['M'+str(master_row)].border = thin_border
    work_sheet['M'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['N'+str(master_row)] = value["weeks"][1]
    work_sheet['N'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['N'+str(master_row)].border = thin_border
    work_sheet['N'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['O'+str(master_row)] = value["weeks"][2]
    work_sheet['O'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['O'+str(master_row)].border = thin_border
    work_sheet['O'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['P'+str(master_row)] = value["weeks"][3]
    work_sheet['P'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['P'+str(master_row)].border = thin_border
    work_sheet['P'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['Q'+str(master_row)] = value["weeks"][4]
    work_sheet['Q'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['Q'+str(master_row)].border = thin_border
    work_sheet['Q'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['R'+str(master_row)] = value['Minutes']
    work_sheet['R'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['R'+str(master_row)].border = thin_border
    work_sheet['R'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['S'+str(master_row)] = value['Hours: Minutes']
    work_sheet['S'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['S'+str(master_row)].border = thin_border
    work_sheet['S'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['T'+str(master_row)] = value['Count']
    work_sheet['T'+str(master_row)].fill = PatternFill(patternType='solid', fgColor='C0C0C0')
    work_sheet['T'+str(master_row)].border = thin_border
    work_sheet['T'+str(master_row)].alignment = center_allign

    master_row += 1

def quote_summary_table(work_sheet, summaryJSON):
    global master_row

    work_sheet.font = bold_font_11
    for key, value in summaryJSON.items():
        # print(key, value, end="\t")
        # print(value[0], end="\n\n")

        work_sheet['B'+str(master_row)] = key
        work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['B'+str(master_row)].border = thin_border

        work_sheet['C'+str(master_row)] = value["weeks"][0]
        work_sheet['C'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['C'+str(master_row)].border = thin_border
        work_sheet['C'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['D'+str(master_row)] = value["weeks"][1]
        work_sheet['D'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['D'+str(master_row)].border = thin_border
        work_sheet['D'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['E'+str(master_row)] = value["weeks"][2]
        work_sheet['E'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['E'+str(master_row)].border = thin_border
        work_sheet['E'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['F'+str(master_row)] = value["weeks"][3]
        work_sheet['F'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['F'+str(master_row)].border = thin_border
        work_sheet['F'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['G'+str(master_row)] = value["weeks"][4]
        work_sheet['G'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['G'+str(master_row)].border = thin_border
        work_sheet['G'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['H'+str(master_row)] = value['Minutes']
        work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['H'+str(master_row)].border = thin_border
        work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['I'+str(master_row)] = value['Hours: Minutes']
        work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['I'+str(master_row)].border = thin_border
        work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

        work_sheet['J'+str(master_row)] = value['Count']
        work_sheet['J'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
        work_sheet['J'+str(master_row)].border = thin_border
        work_sheet['J'+str(master_row)].alignment = center_allign

        master_row += 1

    # value = summaryJSON['Summary']
    # work_sheet['B'+str(master_row)] = 'Total'
    # work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['B'+str(master_row)].border = thin_border
    
    # work_sheet['C'+str(master_row)] = value["weeks"][0]
    # work_sheet['C'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['C'+str(master_row)].border = thin_border
    # work_sheet['C'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    # work_sheet['D'+str(master_row)] = value["weeks"][1]
    # work_sheet['D'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['D'+str(master_row)].border = thin_border
    # work_sheet['D'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    # work_sheet['E'+str(master_row)] = value["weeks"][2]
    # work_sheet['E'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['E'+str(master_row)].border = thin_border
    # work_sheet['E'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    # work_sheet['F'+str(master_row)] = value["weeks"][3]
    # work_sheet['F'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['F'+str(master_row)].border = thin_border
    # work_sheet['F'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    # work_sheet['G'+str(master_row)] = value["weeks"][4]
    # work_sheet['G'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['G'+str(master_row)].border = thin_border
    # work_sheet['G'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    # work_sheet['H'+str(master_row)] = value['Minutes']
    # work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['H'+str(master_row)].border = thin_border
    # work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    # work_sheet['I'+str(master_row)] = value['Hours: Minutes']
    # work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['I'+str(master_row)].border = thin_border
    # work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    # work_sheet['J'+str(master_row)] = value['Count']
    # work_sheet['J'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    # work_sheet['J'+str(master_row)].border = thin_border
    # work_sheet['J'+str(master_row)].alignment = center_allign
    # --------------------------------------------- Quotes summary Table Data End ---------------------------------------------

    master_row += 1

def time_utilization_for_month(work_sheet, summaryJSON):
    global master_row

    work_sheet.merge_cells('B{row}:I{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'TIME UTILIZATION FOR MONTH'
    cell = work_sheet['B'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, 'B48D00'])

    master_row += 1

    work_sheet['B'+str(master_row)] = 'Minutes'
    cell = work_sheet['B'+str(master_row)]
    cell.font = bold_font_11
    cell.fill = PatternFill(patternType='solid', fgColor='9C7900')
    cell.alignment = Alignment(horizontal="right")
    cell.border = thin_border

    work_sheet['I'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['I'+str(master_row)]
    cell.font = bold_font_11
    cell.alignment = Alignment(horizontal="right")
    cell.border = thin_border

    master_row += 1

    work_sheet.merge_cells('B{row}:G{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total Quotes'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['H'+str(master_row)] = summaryJSON['totalQuotesMinutes']
    work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['H'+str(master_row)].border = thin_border
    work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['I'+str(master_row)] = summaryJSON['totalQuotesHours']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:G{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total BO Tasks'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['H'+str(master_row)] = summaryJSON['totalBOTasksMinutes']
    work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['H'+str(master_row)].border = thin_border
    work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['I'+str(master_row)] = summaryJSON['totalBOTasksHours']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:G{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total Utilized'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['H'+str(master_row)] = summaryJSON['totalUtilizedMinutes']
    work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['H'+str(master_row)].border = thin_border
    work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['I'+str(master_row)] = summaryJSON['totalUtilizedHours']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")


    master_row += 1

    work_sheet.merge_cells('B{row}:G{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Available Time for 4 FTs - (160 Hrs x 4) '
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['H'+str(master_row)] = summaryJSON['availableTimeMinutes']
    work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['H'+str(master_row)].border = thin_border
    work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['I'+str(master_row)] = summaryJSON['availableTimeHours']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:G{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Unused'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['H'+str(master_row)] = summaryJSON['unusedTimeMinutes']
    work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['H'+str(master_row)].border = thin_border
    work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['I'+str(master_row)] = summaryJSON['unusedTimeHours']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:G{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Extra'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['H'+str(master_row)] = summaryJSON['extraMinutes']
    work_sheet['H'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['H'+str(master_row)].border = thin_border
    work_sheet['H'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    work_sheet['I'+str(master_row)] = summaryJSON['extraTimeHours']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

def task_utilization_for_month(work_sheet, summaryJSON):
    global master_row

    work_sheet.merge_cells('B{row}:I{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'TASK UTILIZATION FOR MONTH'
    cell = work_sheet['B'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, '000F5C'])

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total Households Quoted'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['totalHouseholdsQuoted']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total BO Tasks Completed'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['totalBOTasksCompleted']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total Applications Created'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['totalApplicationsCreated']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total Policies Audited'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['totalPoliciesAudited']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Total LOBs Quoted'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['totalLOBQuoted']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1
    




    work_sheet.merge_cells('B{row}:I{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Count of Quotes According to Priority'
    cell = work_sheet['B'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=['172361', '172361'])

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'No. of High Priority Quotes Completed'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['hpQuotes']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'No. of Medium Priority Quotes Completed'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['mpQuotes']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'No. of Low Priority Quotes Completed'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['lpQuotes']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1





    work_sheet.merge_cells('B{row}:I{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'Count of BO Tasks According to Priority'
    cell = work_sheet['B'+str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, 'A6ACAF'])

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'No. of High Priority BO Tasks Completed'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['hpBOTasks']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'No. of Medium Priority BO Tasks Completed'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['mpBOTasks']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

    work_sheet.merge_cells('B{row}:H{row}'.format(row=str(master_row)))
    work_sheet['B'+str(master_row)] = 'No. of Low Priority BO Tasks Completed'
    work_sheet['B'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['B'+str(master_row)].border = thin_border

    work_sheet['I'+str(master_row)] = summaryJSON['lpBOTasks']
    work_sheet['I'+str(master_row)].fill = PatternFill(patternType='solid', fgColor=WHITE)
    work_sheet['I'+str(master_row)].border = thin_border
    work_sheet['I'+str(master_row)].alignment = Alignment(horizontal="right" , vertical="bottom")

    master_row += 1

def agent_utilization(work_sheet, summaryJSON):
    global master_row

    work_sheet.merge_cells('M{row}:S{row}'.format(row=str(master_row)))
    work_sheet['M' + str(master_row)] = 'Agent Utilization'
    cell = work_sheet['M' + str(master_row)]
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.fill = GradientFill(type='linear', degree=90, left=0, right=1, top=1, bottom=0, stop=[WHITE, 'A6ACAF'])

    master_row += 1

    work_sheet['M'+str(master_row)] = 'Agent/CSR'
    cell = work_sheet['M'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.border = thin_border

    work_sheet['N'+str(master_row)] = 'Quote Time (Mins)'
    cell = work_sheet['N'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.border = thin_border
    
    work_sheet['O'+str(master_row)] = 'BO Time (Mins)'
    cell = work_sheet['O'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['P'+str(master_row)] = 'Total Time (in Mins)'
    cell = work_sheet['P'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['Q'+str(master_row)] = 'Hours: Minutes'
    cell = work_sheet['Q'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['R'+str(master_row)] = 'Quotes Count'
    cell = work_sheet['R'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.border = thin_border

    work_sheet['S'+str(master_row)] = 'BO Tasks Count'
    cell = work_sheet['S'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='C0392B')
    cell.font = Font(size=18, color='FFFFFF', bold=True) 
    cell.alignment = center_allign
    cell.border = thin_border

    master_row += 1

    work_sheet.font = bold_font_11
    for key, value in summaryJSON['agent'].items():
        work_sheet['M'+str(master_row)] = key
        cell = work_sheet['M'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.border = thin_border

        work_sheet['N'+str(master_row)] = value['quoteTime']
        cell = work_sheet['N'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border
        
        work_sheet['O'+str(master_row)] = value['BOTime']
        cell = work_sheet['O'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['P'+str(master_row)] = value['totalTime']
        cell = work_sheet['P'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['Q'+str(master_row)] = value['Hours: Minutes']
        cell = work_sheet['Q'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['R'+str(master_row)] = value['quotesCount']
        cell = work_sheet['R'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        work_sheet['S'+str(master_row)] = value['BOTasksCount']
        cell = work_sheet['S'+str(master_row)]
        cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
        cell.alignment = Alignment(horizontal="right" , vertical="bottom")
        cell.border = thin_border

        master_row += 1

    value = summaryJSON['Summary']
    work_sheet['M'+str(master_row)] = 'Total'
    cell = work_sheet['M'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
    cell.border = thin_border

    work_sheet['N'+str(master_row)] = value['quoteTime']
    cell = work_sheet['N'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.border = thin_border
    
    work_sheet['O'+str(master_row)] = value['BOTime']
    cell = work_sheet['O'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.border = thin_border

    work_sheet['P'+str(master_row)] = value['totalTime']
    cell = work_sheet['P'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.border = thin_border

    work_sheet['Q'+str(master_row)] = value['Hours: Minutes']
    cell = work_sheet['Q'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.border = thin_border

    work_sheet['R'+str(master_row)] = value['quotesCount']
    cell = work_sheet['R'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.border = thin_border

    work_sheet['S'+str(master_row)] = value['BOTasksCount']
    cell = work_sheet['S'+str(master_row)]
    cell.fill = PatternFill(patternType='solid', fgColor='FFFFFF')
    cell.alignment = Alignment(horizontal="right" , vertical="bottom")
    cell.border = thin_border

    master_row += 1

def write_summary_sheet(work_sheet, agency_name, timesheet, agency_agents, category_type_description):
    global master_row

    summaryJSON = getTotalSummaryData(timesheet, category_type_description, agency_agents)
    master_row = 5

    header(work_sheet)
    quote_summary_table_header(work_sheet)
    quote_summary_table(work_sheet, summaryJSON[0]["Customer Type"])
    quote_summary_table(work_sheet, summaryJSON[1]["lineOfBusiness"])
    master_row += 1

    time_utilization_for_month(work_sheet, summaryJSON[3])
    master_row += 2
    task_utilization_for_month(work_sheet, summaryJSON[4])

    master_row = 5
    bo_summary_table_header(work_sheet)
    bo_summary_table(work_sheet, summaryJSON[2])
    master_row += 1
    agent_utilization(work_sheet, summaryJSON[5])

 
wb = load_workbook(sys.argv[1], data_only=True)
timesheet = readTimesheet(wb["Month"])
agency_agents, category_type_description = readData(wb["Data"])

agency_name = "Excellent Service Insurance Agency"
dest_filename = 'op_summary' + str(randint(0, 99)) + ".xlsx"
worksheet = wb.create_sheet('Summary')    
write_summary_sheet(worksheet, agency_name, timesheet, agency_agents, category_type_description)
wb.save(filename = dest_filename)
