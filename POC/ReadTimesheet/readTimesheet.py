from openpyxl import load_workbook
import pprint   
import sys

# Loading the xlsx file

wb = load_workbook(sys.argv[1], data_only=True, read_only=True)
month = wb["Month"]

# Global Variables
headerRow = None
headerFound = False
rows = []

for row in month.iter_rows(min_row=1, max_col=13, values_only=True):
    # Save the header row
    if(row[0]=="Date"):
        headerRow = row
        headerFound = True
        print("Header Found  : ", headerRow)
        continue
        
    # Skipping the next row after header row as it is merged
    if headerRow:
        if headerFound:
            headerFound = False
            continue

        # Read row until row[0] is not None
        if row[0] != None:
            tempRow = {}        
            # Creating a dict of colName : Value
            for i in range(0, len(row)):
                tempRow[headerRow[i]] = row[i]        
            rows.append(tempRow)

pp = pprint.PrettyPrinter(indent=4)
pp.pprint(rows)
