AGENCIES = {
    "RWA": "Roger Welch Agency",
    "EIA": "Excellent Service Insurance Agency",
    "JLA": "James Little Agency",
    "OIA": "Orbit Insurance Agency",
    "TSIA": "McShan Agency LLC",
    "CoVerica": "CoVerica",
    "Internal_NonBillable": "Internal_NonBillable",
    "TBIS": "Term Brokers Insurance"
}

SUMMARY_AVAILABLE_TIME = {
    "EIA": 2400,
    "TSIA": 2400,
    "RWA": 2400,
    "OIA": 38400,
    "JLA": 2400,
    "Internal_NonBillable": 2400,
    "TBIS": 38400
}

AVAILABLE_TIME = {
    "OIA": "Available Time for 4 FTs - (160 Hrs x 4)",
    "EIA": "Available Time",
    "TSIA": "Available Time",
    "RWA": "Available Time",
    "JLA": "Available Time",
    "Internal_NonBillable": "Available Time",
    "TBIS": "Available Time for 4 FTs - (160 Hrs x 4)"
}
