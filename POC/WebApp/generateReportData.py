from os import times
import string
from openpyxl import load_workbook
import sys
import json
from datetime import date, datetime
from copy import deepcopy
import constants
# Will be initialized in getWeekIndex when reading Timesheet
GLOBAL_DATE = None

# AVAILABLE TIME IS DIFFERENT FOR ALL AGENCIES
summary_available_time = constants.SUMMARY_AVAILABLE_TIME

#  Reading the excel sheet
# wb = load_workbook(sys.argv[1], data_only=True)

# -------------------------------------------- Utilities
def printJSON(data):
    print(json.dumps(data, indent=4, default=str))

def convertMinutesToTimeString(minutes):
    hours = minutes // 60
    minutes = minutes % 60
    return "{}:{}".format(hours, minutes)


def getWeekIndex(row): # 0 : 1st Week, 1: 2nd Week.
    global GLOBAL_DATE
    dt = row["Date"]
    if type(dt) == string :
        dt = datetime.fromisoformat(dt)
    if GLOBAL_DATE == None:
        GLOBAL_DATE = dt
    now = date(dt.year, dt.month, 1)

    ls = {}
    ls_lit = []
    weekCounter = 0
    weekCountIncremented = False
    for i in range(1, 32):
        try:
            thisdate = date(now.year, now.month, i)
        except(ValueError):
            break
        if thisdate.weekday() < 5: # Monday == 0, Sunday == 6 
            if i == dt.day:
                return weekCounter
            ls_lit.append(thisdate.day)
            weekCountIncremented = False
        else:
            if ls_lit:
                ls[weekCounter] = ls_lit
                weekCounter += 1
            ls_lit = []
            if not weekCountIncremented : 
                weekCountIncremented = True
    # Invalid date
    return None

def getWeekDates():
    global GLOBAL_DATE
    now = date(GLOBAL_DATE.year, GLOBAL_DATE.month, 1)
    ls = {}
    ls_lit = []
    weekCounter = 0
    weekCountIncremented = False
    for i in range(1, 32):
        try:
            thisdate = date(now.year, now.month, i)
        except(ValueError):
            break
        if thisdate.weekday() < 5: # Monday == 0, Sunday == 6 
            ls_lit.append(thisdate.day)
            weekCountIncremented = False
        else:
            if ls_lit:
                ls[weekCounter] = ls_lit
                weekCounter += 1
            ls_lit = []
            if not weekCountIncremented : 
                weekCountIncremented = True

    if ls_lit:
        ls[weekCounter] = ls_lit
    return (ls)

# -------------------------------------------- Read DATA
def readTimesheet(month):
    # Global Variables
    headerRow = None
    headerFound = False
    headerRowForAgentWiseRows = None
    headerFoundForAgentWiseRows = False
    agentWiseRows = {}
    for row in month.iter_rows(min_row=1, max_col=13, values_only=True):
        if(row[0]=="Date"):
            headerRowForAgentWiseRows = row
            headerFoundForAgentWiseRows = True
            headerRow = row
            # print("Header Found  : ", headerRow)
            continue

        if headerRowForAgentWiseRows:
            if headerFoundForAgentWiseRows:
                headerFoundForAgentWiseRows = False
                continue
            
            agencyName = row[2]
            if agencyName ==  None:
                continue

            if agencyName not in agentWiseRows:
                agentWiseRows[agencyName] = []
            
            agentWiseRows[agencyName].append(row)

    # printJSON(agentWiseRows)
    for agency, data in agentWiseRows.items():
        rows = {}        
        # printJSON(data)
        for row in data: #month.iter_rows(min_row=1, max_col=13, values_only=True):
            # Save the header row
            # if(row[0]=="Date"):
            #     headerRow = row
            #     headerFound = True
            #     # print("Header Found  : ", headerRow)
            #     continue
                
            # Skipping the next row after header row as it is merged
            # if headerRow:
            #     if headerFound:
            #         headerFound = False
            #         continue

            # Read row until row[0] is not None
            if row[0] != None:
                tempRow = {}        
                # Creating a dict of colName : Value
                for i in range(0, len(row)):
                    tempRow[headerRow[i]] = 0 if row[i] == None else row[i]       
                
                rowWeek = getWeekIndex(tempRow)
                if rowWeek in rows:
                    rows[getWeekIndex(tempRow)].append(tempRow)
                else:
                    rows[rowWeek] = [tempRow]
        agentWiseRows[agency] = rows

    
    # with open('dataLog.txt', 'w') as file:
    #     file.write(json.dumps(agentWiseRows, indent=4, sort_keys=True, default=str))
    # printJSON(agentWiseRows)
    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(rows)
    return agentWiseRows

def readData(data):
    # Global Variables
    agency_agents = {}
    category_type_description = {}
    currentSection = 0

    for column_cell in data.iter_cols(1, data.max_column):  # iterate column cell
        
        # Check if column is None, Gaps in columns to seperate the types
        if column_cell[0].value == None:
            currentSection += 1
            continue
        
        # ----------------------- Building agency_agents
        if currentSection == 0: 
            colName = column_cell[0].value
            agency_agents[colName] = []
            # Read Column rows
            for row in column_cell[1:]:    # iterate  column
                #  Read Till last data row
                if row.value == None:
                    break
                agency_agents[colName].append(row.value)

        # ----------------------- Building category & type
        elif currentSection == 1:
            category = column_cell[0].value
            category_type_description[category] = {}
            # ----------------------- Building type
            # Read Column rows
            for typeName in column_cell[1:]:    # iterate  column
                #  Read Till last data row
                if typeName.value == None:
                    break
                category_type_description[category][typeName.value] = []

        # ----------------------- Building description # TODO considering that the types are unique, might break if type is duplicated
        else:
            typeName = column_cell[0].value
            for key in category_type_description:
                # Search for the typeName under all categories
                if typeName in category_type_description[key]:
                    # typeName Found !
                    for description in column_cell[1:]:    # iterate  column
                        #  Read Till last data row
                        if description.value == None:
                            break
                        category_type_description[key][typeName].append(description.value)

    # print(json.dumps(agency_agents, indent=4))
    # print(json.dumps(category_type_description, indent=4))
    return agency_agents, category_type_description

# -------------------------------------------- Generate Report Data
# Generating QuoteSummary
def getQuoteSummary(timesheet, category_type_description):
    totalCount = 0
    quoteFields = category_type_description["Quote"]
    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0",
        "Count": 0
    }
    for key in quoteFields:
        quoteFields[key] = dataFields.copy()

    
    for row in timesheet:

        if row["Type"] in quoteFields:
            quoteFields[row["Type"]]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            quoteFields[row["Type"]]["Hours: Minutes"] = convertMinutesToTimeString(quoteFields[row["Type"]]["Minutes"])
            quoteFields[row["Type"]]["Count"] += 1
            totalCount += 1
           
    quoteSummary = {
        "Customer Type": quoteFields,
        "Summary":{
            "totalCount" : totalCount
        }
    }

    """
    {
        "Customer Type": {
            "NC_Brand_New": {
                "week":[]
                "Minutes": 70,
                "Hours: Minutes": "1:10",
                "Count": 1
            },
            "NC_Returning": {
                "Minutes": 0,
                "Hours: Minutes": 0,
                "Count": 0
            },
            "EC_New_LOB": {
                "Minutes": 0,
                "Hours: Minutes": 0,
                "Count": 0
            },
            "EC_Renewal_Requote": {
                "Minutes": 0,
                "Hours: Minutes": 0,
                "Count": 0
            }
        },
        "Summary": {
            "totalCount": 1
        }
    }
    """
    return quoteSummary

def getQuoteDetailedResult(timesheet, category_type_description):
    totalMinutesCount = 0
    totalHoursMinutesCount = 0
    totalCount = 0
    totalCountPerLOB = 0
    
    highPriorityTasks = 0
    mediumPriorityTasks = 0
    lowPriorityTasks = 0

    LOBFieldsList = []
    LOBFields = {}
    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0",
        "Count": 0,
        "Count Per LOB": 0
    }
    # Getting LOB data
    for key in category_type_description["Quote"]:
        LOBFieldsList = category_type_description["Quote"][key]
        break

    for value in LOBFieldsList:
        LOBFields[value] = dataFields.copy()

    # Filling LOB table data
    for row in timesheet:
        if row["Category"] == "Quote":
            if row["Quote/Task Priority"] == "High":
                highPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Medium":
                mediumPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Low":
                lowPriorityTasks += 1

        if row["Description"] in LOBFields:
            LOBFields[row["Description"]]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            LOBFields[row["Description"]]["Hours: Minutes"] = convertMinutesToTimeString(LOBFields[row["Description"]]["Minutes"])
            LOBFields[row["Description"]]["Count"] += 1

            if row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"] != None:
                LOBFields[row["Description"]]["Count Per LOB"] += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]
                totalCountPerLOB += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]

            totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]
            totalCount += 1
    
    totalHoursMinutesCount = convertMinutesToTimeString(totalMinutesCount)

    quoteDetailedResult = {
        "Line of Business": LOBFields,
        "Summary":{
            "totalMinutesCount": totalMinutesCount,
            "totalHoursMinutesCount": totalHoursMinutesCount,
            "totalCount": totalCount,
            "totalCountPerLOB": totalCountPerLOB,
            "highPriorityTasks": highPriorityTasks,
            "mediumPriorityTasks": mediumPriorityTasks,
            "lowPriorityTasks": lowPriorityTasks
        }
    }
    """
        {
            "Line of Business": {
                "Auto": {
                    "Minutes": 264,
                    "Hours: Minutes": "4:24",
                    "Count": 5,
                    "Count Per LOB": 5
                },
                "Home": {
                    "Minutes": 654,
                    "Hours: Minutes": "10:54",
                    "Count": 11,
                    "Count Per LOB": 14
                },
                "Renters": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Condo": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Home": {
                    "Minutes": 2700,
                    "Hours: Minutes": "45:0",
                    "Count": 26,
                    "Count Per LOB": 58
                },
                "Auto+Renters": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Condo": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Home+DF": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Dwelling_Fire": {
                    "Minutes": 845,
                    "Hours: Minutes": "14:5",
                    "Count": 14,
                    "Count Per LOB": 14
                },
                "Landlord_Condo": {
                    "Minutes": 275,
                    "Hours: Minutes": "4:35",
                    "Count": 5,
                    "Count Per LOB": 5
                },
                "Umbrella": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Home+Umbrella": {
                    "Minutes": 730,
                    "Hours: Minutes": "12:10",
                    "Count": 7,
                    "Count Per LOB": 27
                },
                "Auto+Umbrella": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Auto+Renters+Umbrella": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "EZLynx_Quick_Review": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Motorcycle": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Trailer": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Vacant_Dwelling": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Earthquake": {
                    "Minutes": 28,
                    "Hours: Minutes": "0:28",
                    "Count": 1,
                    "Count Per LOB": 1
                },
                "Boat": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Flood": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Commercial": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                },
                "Life_Quote": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0,
                    "Count Per LOB": 0
                }
            },
            "Summary": {
                "totalMinutesCount": 5496,
                "totalHoursMinutesCount": "91:36",
                "totalCount": 69,
                "totalCountPerLOB": 124
            }
        }
    """
    return quoteDetailedResult

def getBOTasksSummary(timesheet, category_type_description):
    totalMinutesCount = 0
    totalHoursMinutesCount = 0
    totalCount = 0
    
    highPriorityTasks = 0
    mediumPriorityTasks = 0
    lowPriorityTasks = 0

    BOTasks = {}
    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0",
        "Count": 0
    }


    # Build Placeholder
    for key in category_type_description["BO"]:
        for task in category_type_description["BO"][key]:
            BOTasks[task] = dataFields.copy()

    # Filling LOB table data
    for row in timesheet:
        if row["Category"] == "BO":
            if row["Quote/Task Priority"] == "High":
                highPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Medium":
                mediumPriorityTasks += 1
            elif row["Quote/Task Priority"] == "Low":
                lowPriorityTasks += 1
            
        if row["Description"] in BOTasks:
            BOTasks[row["Description"]]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            BOTasks[row["Description"]]["Hours: Minutes"] = convertMinutesToTimeString(BOTasks[row["Description"]]["Minutes"])
            BOTasks[row["Description"]]["Count"] += 1

            totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]
            totalCount += 1

    totalHoursMinutesCount = convertMinutesToTimeString(totalMinutesCount)
    BOTasksSummary = {
        "BO Tasks": BOTasks,
        "Summary":{
            "totalMinutesCount": totalMinutesCount,
            "totalHoursMinutesCount": totalHoursMinutesCount,
            "totalCount": totalCount,
            "highPriorityTasks": highPriorityTasks,
            "mediumPriorityTasks": mediumPriorityTasks,
            "lowPriorityTasks": lowPriorityTasks
        }
    }
    """
        {
            "Line of Business": {
                "Application_Creation": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "COI_Printing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "DocuSign": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Audit": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Carrier": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Inbound": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Outbound": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_HawkSoft": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_Other": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Chat_with_Agent": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Chat_with_Team": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_with_Agent": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Call_with_Team": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Download_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Downloads_Run": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Policy_Tab_Creation": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Unmatched_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Email_Sorting": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Email_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Email_Monitoring": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Marketing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Referral": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Touch_Point": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Miscellaneous_Billable": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Change_Mortgage_Lien": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Change_Policy": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Policy_Issue": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Payment_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Quote_Request_Creation": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_Agency_Growth": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_EzLynx": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_HawkSoft": {
                    "Minutes": 1500,
                    "Hours: Minutes": "25:0",
                    "Count": 100
                },
                "Reports_Monthly_Renewal": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_Policy_Activity": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Reports_Policy_Count": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Suspense_Processing": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Tech_Support_To_Agent": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                },
                "Quote_Review": {
                    "Minutes": 0,
                    "Hours: Minutes": "0:0",
                    "Count": 0
                }
            },
            "Summary": {
                "totalMinutesCount": 1500,
                "totalHoursMinutesCount": "25:0",
                "totalCount": 100
            }
        }
    """
    return BOTasksSummary

def getUtilizationForWeek(timesheet, category_type_description):    
    totalMinutesCount = 0
    totalHoursMinutesCount = 0

    dataFields = {
        "Minutes": 0,
        "Hours: Minutes": "0:0"        
    }

    category = {
        "Application_Creation": dataFields.copy(),
        "Audit": dataFields.copy(),
        "Total Quotes": dataFields.copy(),
        "Total BO Tasks": dataFields.copy()
    }

    # Filling category table data
    for row in timesheet:
        if "Application_Creation" in row["Description"]:
            category["Application_Creation"]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            category["Application_Creation"]["Hours: Minutes"] = convertMinutesToTimeString(category["Application_Creation"]["Minutes"])

            # totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]
        elif "Audit" in row["Description"]:
            category["Audit"]["Minutes"] += row["Total Time taken by quote/task (in Minutes)"]
            category["Audit"]["Hours: Minutes"] = convertMinutesToTimeString(category["Audit"]["Minutes"])

            # totalMinutesCount += row["Total Time taken by quote/task (in Minutes)"]

    quotesData = getQuoteDetailedResult(timesheet, category_type_description)

    category["Total Quotes"]["Minutes"] = quotesData["Summary"]["totalMinutesCount"]
    category["Total Quotes"]["Hours: Minutes"] = quotesData["Summary"]["totalHoursMinutesCount"]
    totalMinutesCount += quotesData["Summary"]["totalMinutesCount"]

    BOdata = getBOTasksSummary(timesheet, category_type_description)

    category["Total BO Tasks"]["Minutes"] = BOdata["Summary"]["totalMinutesCount"]
    category["Total BO Tasks"]["Hours: Minutes"] = BOdata["Summary"]["totalHoursMinutesCount"]
    totalMinutesCount += BOdata["Summary"]["totalMinutesCount"]

    totalHoursMinutesCount = convertMinutesToTimeString(totalMinutesCount)

    categoryData = {
        "Category": category,
        "Summary":{
            "Consumed Time-Minute for Week": totalMinutesCount,
            "Consumed Time-Hour for Week": totalHoursMinutesCount
        }
    }

    """
    {
        "Category": {
            "Application_Creation": {
                "Minutes": 0,
                "Hours: Minutes": "0:0"
            },
            "Audit": {
                "Minutes": 0,
                "Hours: Minutes": "0:0"
            },
            "Total Quotes": {
                "Minutes": 5496,
                "Hours: Minutes": "91:36"
            },
            "Total BO Tasks": {
                "Minutes": 1500,
                "Hours: Minutes": "25:0"
            }
        },
        "Summary": {
            "Consumed Time-Minute for Week": 6996,
            "Consumed Time-Hour for Week": "116:36"
        }
    }
    """

    return categoryData

def getCountTable(timesheet, category_type_description):
    quoteDetailedResult = getQuoteDetailedResult(timesheet, category_type_description)
    BOTasksSummary = getBOTasksSummary(timesheet, category_type_description)
    
    countTable = {}
    
    applicationsCreated = 0
    policiesAudited = 0

    for row in timesheet:
        if "Application_Creation" in row["Description"]:
            applicationsCreated += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]

        elif "Audit" in row["Description"]:
            policiesAudited += row["No. of Quotes (1 quote per LOB) / No. of Audits / No. of Applications"]

    countTable["Total Quotes (Households)"] =  quoteDetailedResult["Summary"]["totalCount"]
    countTable["Total Quotes (LOBs)"] =  quoteDetailedResult["Summary"]["totalCountPerLOB"]
    countTable["Total BO Tasks"] =  BOTasksSummary["Summary"]["totalCount"]
    countTable["Applications Created"] =  applicationsCreated
    countTable["Policies Audited"] =  policiesAudited
    countTable["High Priority Quotes"] =  quoteDetailedResult["Summary"]["highPriorityTasks"]
    countTable["Medium Priority Quotes"] =  quoteDetailedResult["Summary"]["mediumPriorityTasks"]
    countTable["Low Priority Quotes"] =  quoteDetailedResult["Summary"]["lowPriorityTasks"]
    countTable["High Priority BO Tasks"] =  BOTasksSummary["Summary"]["highPriorityTasks"]
    countTable["Medium Priority BO Tasks"] =  BOTasksSummary["Summary"]["mediumPriorityTasks"]
    countTable["Low Priority BO Tasks"] =  BOTasksSummary["Summary"]["lowPriorityTasks"]

    """
    {
        "Total Quotes (Households)": 69,
        "Total Quotes (LOBs)": 124,
        "Total BO Tasks": 100,
        "Applications Created": 0,
        "Policies Audited": 0,
        "High Priority Quotes": 43,
        "Medium Priority Quotes": 0,
        "Low Priority Quotes": 26,
        "High Priority BO Tasks": 0,
        "Medium Priority BO Tasks": 100,
        "Low Priority BO Tasks": 0
    }
    """
    return countTable

def getAgentUtilization(timesheet, agency_agents):
    agency = timesheet[0]["Agency"]
    agents = agency_agents[agency]

    agentUtilization = {}

    dataFields = {
        "quoteTime": 0,
        "BOTime": 0,
        "totalTime": 0,
        "Hours: Minutes": "0:0",
        "quotesCount": 0,
        "BOTasksCount": 0
    }

    # Build Placeholder
    for agent in agents:
        agentUtilization[agent] = dataFields.copy()
        
    for row in timesheet:
        if row["Agent/Producer/CSR"] in agents:
            if row["Category"] == "Quote":
                agentUtilization[row["Agent/Producer/CSR"]]["quoteTime"] += row["Total Time taken by quote/task (in Minutes)"]    
                agentUtilization[row["Agent/Producer/CSR"]]["quotesCount"] += 1
            elif row["Category"] == "BO":
                agentUtilization[row["Agent/Producer/CSR"]]["BOTime"] += row["Total Time taken by quote/task (in Minutes)"]    
                agentUtilization[row["Agent/Producer/CSR"]]["BOTasksCount"] += 1

            agentUtilization[row["Agent/Producer/CSR"]]["totalTime"] = agentUtilization[row["Agent/Producer/CSR"]]["quoteTime"] + agentUtilization[row["Agent/Producer/CSR"]]["BOTime"]
            agentUtilization[row["Agent/Producer/CSR"]]["Hours: Minutes"] = convertMinutesToTimeString(agentUtilization[row["Agent/Producer/CSR"]]["totalTime"] )

    """
    {
        "growth.insure": {
            "quoteTime": 2740,
            "BOTime": 0,
            "totalTime": 2740,
            "Hours: Minutes": "45:40",
            "quotesCount": 34,
            "BOTasksCount": 0
        },
        "Karandikar, Smita": {
            "quoteTime": 929,
            "BOTime": 0,
            "totalTime": 929,
            "Hours: Minutes": "15:29",
            "quotesCount": 11,
            "BOTasksCount": 0
        },
        "Natu, Aparna": {
            "quoteTime": 98,
            "BOTime": 0,
            "totalTime": 98,
            "Hours: Minutes": "1:38",
            "quotesCount": 3,
            "BOTasksCount": 0
        },
        "Sathaye, Kiran": {
            "quoteTime": 300,
            "BOTime": 300,
            "totalTime": 600,
            "Hours: Minutes": "10:0",
            "quotesCount": 4,
            "BOTasksCount": 20
        },
        "Sutrave, Seema": {
            "quoteTime": 1409,
            "BOTime": 0,
            "totalTime": 1409,
            "Hours: Minutes": "23:29",
            "quotesCount": 16,
            "BOTasksCount": 0
        }
    }
    """
    return agentUtilization

def getLegends():

    legend = {
        "NC_Brand_New": "Complete Brand New Customer to Agency",		
        "NC_Returning":	"Returning Customer",		
        "EC_New_LOB":	"Existing Customer adding new LOB",		
        "EC_Renewal_Requote":	"Existing Customer - Requote/Remarket/Reshop"		
    }

    return legend

def getTotalSummaryData(timesheet, category_type_description, agency_agents):
    
    summaryData = {
                "weeks":[0,0,0,0,0],
                "Minutes": 0,
                "Hours: Minutes": "",
                "Count": 0
                }

    totalApplicationsCreated = 0
    totalPoliciesAudited = 0
    totalLOBQuoted = 0
    hpQuotes = 0
    mpQuotes = 0
    lpQuotes = 0
    hpBOTasks = 0
    mpBOTasks = 0
    lpBOTasks = 0

    quoteSummaryReturn = None 
    lobSummaryReturn = None 
    BOTasksSummaryReturn = None 
    timeUtilizationReturn = None 
    taskUtilizationReturn = None 
    agentUtilizationReturn = None

    # Quotes Summary
    custType = None
    custTypeSummary = deepcopy(summaryData)

    # Line Of Business
    lob = None
    lobSummary = deepcopy(summaryData)

    # BO Tasks
    BOTasks = None
    BOTasksSummary = deepcopy(summaryData)

    # Agent Wise Utilization
    agentUtilization = None
    agentSummary = None
    agentSummaryData = {
            "quoteTime": 0,
            "BOTime": 0,
            "totalTime": 0,
            "Hours: Minutes": 0,
            "quotesCount": 0,
            "BOTasksCount": 0
        }
    # ------------------------ LOOP STARTS HERE
    for key in getWeekDates():
        if key not in timesheet:
            continue
        # ======================================================================== Quotes Summary
    
        weekQuoteSummary = getQuoteSummary(timesheet[key], deepcopy(category_type_description))
        weekCustType = weekQuoteSummary["Customer Type"]
        
        # Building data first time
        if custType == None:
            custType = deepcopy(weekCustType)
            for custTypeKey in custType:
                custType[custTypeKey] = deepcopy(summaryData)

        for cTypeKey in weekCustType:
            custType[cTypeKey]["weeks"][key] = weekCustType[cTypeKey]["Minutes"]
            custType[cTypeKey]["Minutes"] += weekCustType[cTypeKey]["Minutes"]
            custType[cTypeKey]["Count"] += weekCustType[cTypeKey]["Count"]
            custType[cTypeKey]["Hours: Minutes"] = convertMinutesToTimeString(custType[cTypeKey]["Minutes"])
            
            custTypeSummary["weeks"][key] += weekCustType[cTypeKey]["Minutes"]
            custTypeSummary["Minutes"] += weekCustType[cTypeKey]["Minutes"]
            custTypeSummary["Count"] += weekCustType[cTypeKey]["Count"]
            custTypeSummary["Hours: Minutes"] = convertMinutesToTimeString(custTypeSummary["Minutes"])

        custType["Summary"] = custTypeSummary
        quoteSummaryReturn = {
        "Customer Type": custType,
        "Summary": custTypeSummary
        }

        # ======================================================================== LOB Summary
        
        lobWeekSummary = getQuoteDetailedResult(timesheet[key],  deepcopy(category_type_description))
        lobWeek = lobWeekSummary["Line of Business"]
        
        # Building data first time
        if lob == None:
            lob = deepcopy(lobWeek)
            for lobKey in lob:
                lob[lobKey] = deepcopy(summaryData)

        for lobKey in lobWeek:
            lob[lobKey]["weeks"][key] = lobWeek[lobKey]["Minutes"]
            lob[lobKey]["Minutes"] += lobWeek[lobKey]["Minutes"]
            lob[lobKey]["Count"] += lobWeek[lobKey]["Count"]
            lob[lobKey]["Hours: Minutes"] = convertMinutesToTimeString(lob[lobKey]["Minutes"])
            
            lobSummary["weeks"][key] += lobWeek[lobKey]["Minutes"]
            lobSummary["Minutes"] += lobWeek[lobKey]["Minutes"]
            lobSummary["Count"] += lobWeek[lobKey]["Count"]
            lobSummary["Hours: Minutes"] = convertMinutesToTimeString(lobSummary["Minutes"])

        lob["Summary"] = lobSummary
        lobSummaryReturn = {
            "lineOfBusiness": lob,
            "Summary": lobSummary
        }

        # ======================================================================== BO Tasks Summary
        
        BOTasksWeekSummary = getBOTasksSummary(timesheet[key],  deepcopy(category_type_description))
        # printJSON(BOTasksWeekSummary)
        BOTasksWeek = BOTasksWeekSummary["BO Tasks"]
        
        # Building data first time
        if BOTasks == None:
            BOTasks = deepcopy(BOTasksWeek)
            for BOTasksKey in BOTasks:
                BOTasks[BOTasksKey] = deepcopy(summaryData)

        for BOTasksKey in BOTasksWeek:
            BOTasks[BOTasksKey]["weeks"][key] = BOTasksWeek[BOTasksKey]["Minutes"]
            BOTasks[BOTasksKey]["Minutes"] += BOTasksWeek[BOTasksKey]["Minutes"]
            BOTasks[BOTasksKey]["Count"] += BOTasksWeek[BOTasksKey]["Count"]
            BOTasks[BOTasksKey]["Hours: Minutes"] = convertMinutesToTimeString(BOTasks[BOTasksKey]["Minutes"])
            
            BOTasksSummary["weeks"][key] += BOTasksWeek[BOTasksKey]["Minutes"]
            BOTasksSummary["Minutes"] += BOTasksWeek[BOTasksKey]["Minutes"]
            BOTasksSummary["Count"] += BOTasksWeek[BOTasksKey]["Count"]
            BOTasksSummary["Hours: Minutes"] = convertMinutesToTimeString(BOTasksSummary["Minutes"])

        BOTasksSummaryReturn = {
            "taskType": BOTasks,
            "Summary": BOTasksSummary
        }

        # ======================================================================== Task Utilization For Month        
        countWeek =  getCountTable(timesheet[key],  deepcopy(category_type_description))
        totalApplicationsCreated += countWeek["Applications Created"]
        totalPoliciesAudited += countWeek["Policies Audited"]
        totalLOBQuoted += lobWeekSummary["Summary"]["totalCountPerLOB"]
        # printJSON(lobWeekSummary)
        hpQuotes += countWeek["High Priority Quotes"]
        mpQuotes += countWeek["Medium Priority Quotes"]
        lpQuotes += countWeek["Low Priority Quotes"]
        hpBOTasks += countWeek["High Priority BO Tasks"]
        mpBOTasks += countWeek["Medium Priority BO Tasks"]
        lpBOTasks += countWeek["Low Priority BO Tasks"]

        # ======================================================================== Agent Wise Utilization  | This should be done last in the loop as continue is present in the if statement      
        agentWeek = getAgentUtilization(timesheet[key], deepcopy(agency_agents))
        if agentUtilization == None:
            agentUtilization = agentWeek.copy()
            agentSummary = agentSummaryData.copy()
            for agent in agentWeek:
                agentUtilization[agent] = agentSummaryData.copy()

        
        for agent in agentWeek:
            agentUtilization[agent]["quoteTime"] += agentWeek[agent]["quoteTime"]
            agentUtilization[agent]["BOTime"] += agentWeek[agent]["BOTime"]
            agentUtilization[agent]["totalTime"] = agentUtilization[agent]["quoteTime"] + agentUtilization[agent]["BOTime"]
            agentUtilization[agent]["Hours: Minutes"] = convertMinutesToTimeString(agentUtilization[agent]["totalTime"])
            agentUtilization[agent]["quotesCount"] += agentWeek[agent]["quotesCount"]
            agentUtilization[agent]["BOTasksCount"] += agentWeek[agent]["BOTasksCount"]

            agentSummary["quoteTime"] += agentWeek[agent]["quoteTime"]
            agentSummary["BOTime"] += agentWeek[agent]["BOTime"]
            agentSummary["totalTime"] = agentSummary["quoteTime"] + agentSummary["BOTime"]
            agentSummary["Hours: Minutes"] = convertMinutesToTimeString(agentSummary["totalTime"])
            agentSummary["quotesCount"] += agentWeek[agent]["quotesCount"]
            agentSummary["BOTasksCount"] += agentWeek[agent]["BOTasksCount"]
        
        agentUtilizationReturn = {
        "agent": agentUtilization,
        "Summary": agentSummary
        }

    # ----------------------------------------------------------------------------------------------- LOOP ENDS
    # --------------------------------------------------------------- TIME UTILIZATION FOR MONTH
    totalQuotes = lobSummaryReturn["Summary"]["Minutes"]
    totalBOTasks = BOTasksSummaryReturn["Summary"]["Minutes"]
    totalUtilized = totalQuotes + totalBOTasks
    # TODO need to make this generic
    
    availableTime = summary_available_time[timesheet[0][0]['Agency']]
    unused = 0
    extra = 0
    if((availableTime - totalUtilized) > 0):
        unused = availableTime - totalUtilized
    else:
        extra = totalUtilized - availableTime

    timeUtilizationReturn = {
        "totalQuotesMinutes": totalQuotes,
        "totalQuotesHours": convertMinutesToTimeString(totalQuotes), 
        "totalBOTasksMinutes": totalBOTasks,
        "totalBOTasksHours": convertMinutesToTimeString(totalBOTasks), 
        "totalUtilizedMinutes": totalUtilized,
        "totalUtilizedHours": convertMinutesToTimeString(totalUtilized), 
        "availableTimeMinutes": availableTime,
        "availableTimeHours": convertMinutesToTimeString(availableTime), 
        "unusedTimeMinutes": unused,
        "unusedTimeHours": convertMinutesToTimeString(unused), 
        "extraMinutes": extra,
        "extraTimeHours": convertMinutesToTimeString(extra), 
    }
    
    # ======================================================================== Task Utilization For Month
    totalHouseholdsQuoted = lobSummaryReturn["Summary"]["Count"]
    totalBOTasksCompleted = BOTasksSummaryReturn["Summary"]["Count"]
    taskUtilizationReturn = {
        "totalHouseholdsQuoted": totalHouseholdsQuoted,
        "totalBOTasksCompleted": totalBOTasksCompleted,
        "totalApplicationsCreated": totalApplicationsCreated,
        "totalPoliciesAudited": totalPoliciesAudited,
        "totalLOBQuoted": totalLOBQuoted,
        "hpQuotes": hpQuotes,
        "mpQuotes": mpQuotes,
        "lpQuotes": lpQuotes,
        "hpBOTasks": hpBOTasks,
        "mpBOTasks": mpBOTasks,
        "lpBOTasks": lpBOTasks,
    }

    return quoteSummaryReturn, lobSummaryReturn, BOTasksSummaryReturn, timeUtilizationReturn, taskUtilizationReturn, agentUtilizationReturn





# ------------------------------------------------------------ MAIN
# Read timesheet and database
# timesheet = readTimesheet(wb["Month"])
# agency_agents, category_type_description = readData(wb["Data"])
# printJSON(timesheet)
# printJSON(getTotalSummaryData(timesheet, category_type_description, agency_agents))

# printJSON(timesheet)
# print(getWeekDates())
# for key in getWeekDates():
#     printJSON(getQuoteDetailedResult(timesheet[key], category_type_description))
    # printJSON(getQuoteSummary(timesheet[key], category_type_description))
# printJSON(getBOTasksSummary(timesheet, category_type_description))
# printJSON(getUtilizationForWeek(timesheet, category_type_description))
# printJSON(getCountTable(timesheet, category_type_description))
# printJSON(getCountTable(timesheet, category_type_description))
# printJSON(getAgentUtilization(timesheet, agency_agents))
# printJSON(getLegends())

